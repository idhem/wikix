#!/usr/bin/env python
import argparse
import getpass
import mwclient
import json
import datetime

from wikix import app, db
from models import JoinRequest
from views import make_user_page


def create_account(jrequest, wiki, username=None):
    """Adds a user to the Mediawiki"""
    full_name = \
        u"{firstname} {lastname}".format(firstname=jrequest.first_name,
                                         lastname=jrequest.last_name)
    desired_username = username or \
                       u"{0}.{1}".format(jrequest.first_name,
                                         jrequest.last_name).replace(' ', '.').lower()
    print 'Desired username is: %s' % desired_username
    # For a data-modifying action we need a token, and the current way
    # to do so, is calling the api two times. In response to first call
    # we get a token that we use it to confirm account creation
    try:
        first_response = wiki.api('createaccount',
                                  name=desired_username,
                                  email=jrequest.email,
                                  realname=full_name,
                                  mailpassword=True,
                                  reason="Automatic on bulk import",
                                  language='en')
        token = first_response['createaccount']['token']
        try:
            second_response = wiki.api('createaccount',
                                       name=desired_username,
                                       email=jrequest.email,
                                       realname=full_name,
                                       mailpassword=True,
                                       reason="Automatic on bulk import",
                                       language='en',
                                       token=token)
            print('Mediawiki account %s successfully created.' % desired_username)
        except mwclient.errors.APIError, error:
            if error.code == 'userexists':
                print('Skipping user creation step for user %s' % desired_username)
            else:
                raise
        user_page = wiki.Pages[u'User:{user}'.format(user=desired_username)]
        categories = ''
        for working_group in jrequest.working_groups.split(','):
            if working_group is not None and working_group.strip() is not '':
                categories += u'\n [[Category:{wg}_Members]] '.format(wg=working_group.capitalize())
        new_page_text = u"{old_text} \n {body} \n {categories}".format(
            old_text=u"{{warning| This page is auto-generated, do not edit.}}",#user_page.text()
            body=make_user_page(jrequest),
            categories=categories)
        user_page.save(new_page_text)
        jrequest.mw_username = desired_username
        user_profile_url = \
            u'http://{host}{path}/index.php/User:{user}'.format(host=app.config['MEDIA_WIKI'],
                                                                path=app.config['MEDIA_WIKI_PATH'],
                                                                user=desired_username)
        jrequest.mw_profile = user_profile_url
        print user_page
        db.session.commit()
        print('Mediawiki profile page for user %s successfully created.' % desired_username)
    except Exception, error:
        print(u'Error updating mediawiki %s' % error)

if __name__ == '__main__':
    """Reads EMMC member information from a JSON file creates corresponding users on a Mediawiki installation.

    Sample usage:
        $ python bulk.py --host wiki-emmc.org --path / --username mehdi ~/test_database.json
    """
    parser = argparse.ArgumentParser()

    parser.add_argument('file', type=str, help='JSON file containing EMMC member information')
    parser.add_argument('--host', type=str, help='Mediawiki host e.g., wiki-emmc.org')
    parser.add_argument('--path', type=str, help='Mediawiki path e.g., /wiki/', default='/')
    parser.add_argument('--username', type=str, help='Mediawiki username')
    parser.add_argument('--password', type=str, help='Mediawiki password')

    args = parser.parse_args()
    if not args.host:
        parser.error('Please enter mediawiki URL')
    password = args.password
    if args.host and not password:
        password = getpass.getpass()

    if args.host and not (args.username and password):
        parser.error('--host should be accompanied by --username and --password')
    try:
        # Connect
        wiki = mwclient.Site(args.host,
                             path=args.path)
        # Login
        wiki.login(args.username, password)
    
        # Parse
        for record in json.load(open(args.file)):
            best_username = None
            # Check for already existing user accounts
            if 'legacy_info' in record:
                legacy_info = record.pop('legacy_info')
                for info in legacy_info:
                    user_name = info['user_name']
                    if ' ' not in user_name and \
                        'test' not in user_name and \
                        'WikiSysop' not in user_name and \
                        'hashibon' not in user_name and \
                        'Hashibon' not in user_name:
                        best_username = user_name
            print 'Best guess for username is: %s' % best_username

            # Create Mediawiki account
            join_request = JoinRequest(**record)
            join_request.submission_date = datetime.datetime.now()
            join_request.approver_user = args.username
            join_request.approve_date = datetime.datetime.now()

            join_request.email_confirmed = True
            join_request.approved = True
            #join_request.approver_user = 'Bulk Importer'
            db.session.add(join_request)
            create_account(join_request, wiki, username=best_username)
            db.session.commit()
    except Exception, error:
        print('Something went wrong: %s' % error)

