import datetime
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import mwclient
from mwclient.errors import LoginError
from flask import render_template, flash, redirect, url_for,\
    request, session, abort
from itsdangerous import URLSafeSerializer, BadSignature

from wikix import app, db
from models import JoinRequest
from forms import JoinForm, LoginForm, get_redirect_target
from decorators import login_required


def _can_approve(groups):
    """Check if the given groups have create user right"""
    if 'bureaucrat' in groups or 'sysop' in groups or 'administrator' in groups:
        return True
    return False


def _is_approver_user():
    """Check if the current user has the right to approve requests"""
    return 'can_approve' in session and session['can_approve']


def make_request(form, jrequest=None):
    if not jrequest:
        jrequest = JoinRequest()
    jrequest.submission_date = datetime.datetime.now()
    jrequest.first_name = form.first_name.data
    jrequest.last_name = form.last_name.data
    jrequest.affiliation = form.affiliation.data
    jrequest.affiliation_country = form.affiliation_country.data
    jrequest.nationality = form.nationality.data
    jrequest.email = form.email.data
    jrequest.phone_number = form.phone_number.data
    jrequest.website = form.website.data
    jrequest.modelling_interests = ','.join(form.modelling_interests.data)
    jrequest.affiliation_type = form.affiliation_type.data
    jrequest.stakeholders = ','.join(form.stakeholders.data)
    jrequest.working_groups = ','.join(form.working_groups.data)
    jrequest.short_bio = form.short_bio.data
    jrequest.message = form.message.data
    return jrequest


def make_user_page(join_request):
    """Creates a wiki page text for a user according to her information.
    """
    from string import Template
    template = Template(u"""
=== Basic Information ===
{| class="wikitable"
|-
| First Name
| [[first_name::$first_name]]
|-
| Last name
| [[last_name::$last_name]]
|-
| Nationality
| [[nationality::$nationality]]
|-
| Email
| [[email::$email]]
|-
| Phone number
| [[phone_number::tel:$phone_number|$phone_number]]
|-
| Website
| [[website::URL::$website]]
|}

=== Affiliation ===
{| class="wikitable"
|-
| Name
| [[affiliation::$affiliation]]
|-
| Country
| [[affiliation_country::$affiliation_country]]
|-
| Type
| [[affiliation_type::$affiliation_type]]
|}

=== Interests ===
{| class="wikitable"
|-
| Modelling interests
| $modelling_interests
|-
| Desired working groups
| $working_groups
|-
| Stakeholders
| $stakeholders
|}

=== Biography ===
$bio
""")
    modelling_interests = []
    for mi in join_request.modelling_interests.split(','):
        modelling_interests.append("[[modelling_interest::{mi}]]".format(mi=mi))
    working_groups = []
    for wg in join_request.working_groups.split(','):
        working_groups.append("[[working_group::{wg}]]".format(wg=wg))
    stakeholders = []
    for sh in join_request.stakeholders.split(','):
        stakeholders.append("[[stakeholders::{sh}]]".format(sh=sh))
    return template.substitute(first_name=join_request.first_name,
                               last_name=join_request.last_name,
                               nationality=join_request.nationality,
                               email=join_request.email,
                               website=join_request.website,
                               phone_number=join_request.phone_number,
                               affiliation=join_request.affiliation,
                               affiliation_country=join_request.affiliation_country,
                               affiliation_type=join_request.affiliation_type,
                               modelling_interests=', '.join(modelling_interests),
                               stakeholders=', '.join(stakeholders),
                               working_groups=', '.join(working_groups),
                               bio=join_request.short_bio)


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if not form.validate_on_submit():
        return render_template('login.html', form=form, next=get_redirect_target())

    wiki = mwclient.Site(app.config['MEDIA_WIKI'],
                         path=app.config['MEDIA_WIKI_PATH'])
    try:
        wiki.login(request.form['username'],
                   request.form['password'])
        session['logged_in'] = True
        session['username'] = request.form['username']
        session['password'] = request.form['password']
        session['can_approve'] = _can_approve(wiki.groups)
        flash('You were logged in')
        return form.redirect('index')
    except LoginError as error:
        result = error.args[1]['result']
        reason = error.args[1]['reason']
        if result in (u'WrongPass', u'NotExists'):
            flash(u'Login information is wrong. Please enter correct MediaWiki account credentials.',
                  category='error')
        else:
            flash(u'Login error: %s: %s' % (result, reason), category='error')
        return redirect(url_for('login'))
    except Exception as error:
        flash(u'Login error: %s' % error, category='error')
        return redirect(url_for('login'))


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    session.pop('username', None)
    session.pop('password', None)
    session.pop('can_approve', None)
    flash('You are now logged out.')
    return redirect(url_for('login'))


@app.route('/', methods=['GET'], defaults={'page': 1})
@app.route('/page<int:page>', methods=['GET'])
@login_required
def index(page):
    jrequests = None
    # Limit query to user's own requests if he/she is not an administrator
    if not _is_approver_user():
        jrequests = \
            JoinRequest.query.filter(JoinRequest.mw_username==session['username']).order_by(JoinRequest.id.desc())
    # Otherwise show everything
    else:
        jrequests = \
            JoinRequest.query.filter().order_by(JoinRequest.approved).order_by(JoinRequest.id.desc())
    pagination = jrequests.paginate(page, app.config['ROWS_PER_PAGE'])
    return render_template('moderate.html',
                           pagination=pagination,
                           requests=pagination.items)


@app.route('/<string:request_id>/')
@login_required
def details(request_id):
    jrequest = JoinRequest.query.get_or_404(request_id)
    if not _is_approver_user() and jrequest.mw_username != session['username']:
        # No mercy for non-admins
        abort(403)
    working_groups = ''
    if jrequest.working_groups is not None:
        working_groups = jrequest.working_groups.split(',')
    return render_template('request.html',
                           request=jrequest,
                           working_groups=working_groups,
                           is_approver=_is_approver_user())


@app.route('/<string:request_id>/delete', methods=['POST'])
@login_required
def delete(request_id):
    if not _is_approver_user():
        abort(403)
    # Loading the request data
    jrequest = JoinRequest.query.get(request_id)
    if jrequest.approved:
        flash('Can not delete an approved join request.', category='danger')
        return redirect(url_for('.details', request_id=request_id))
    else:
        db.session.delete(jrequest)
        db.session.commit()
        flash('Join request #%s deleted.' % request_id, category='success')
        return redirect(url_for('.index'))


@app.route('/<string:request_id>/approve', methods=['POST'])
@login_required
def approve(request_id):
    if not _is_approver_user():
        abort(403)
    # Loading the request data
    jrequest = JoinRequest.query.get(request_id)

    # Communicate with mediawiki installation
    wiki = mwclient.Site(app.config['MEDIA_WIKI'],
                         path=app.config['MEDIA_WIKI_PATH'])
    wiki.login(session['username'],
               session['password'])
    full_name = \
        u"{firstname} {lastname}".format(firstname=jrequest.first_name,
                                         lastname=jrequest.last_name)
    # If this is an update, use the current MediaWiki username
    jrequest.mw_username = jrequest.mw_username or \
        u"{0}.{1}".format(jrequest.first_name,
                          jrequest.last_name).replace(' ', '').lower()
    # For a data-modifying action we need a token, and the current way
    # to do so, is calling the api two times. In response to first call
    # we get a token that we use it to confirm account creation
    try:
        # Do not try to create new account on update requests.
        if not jrequest.is_update:
            query_result = wiki.api('query', meta='tokens', type='createaccount')
            createaccount_token = query_result['query']['tokens']['createaccounttoken']
            try:
                #TODO: authentication flow support is missing, createreturnurl value is fake
                first_response = wiki.api('createaccount',
                                          username=jrequest.mw_username,
                                          email=jrequest.email,
                                          realname=full_name,
                                          mailpassword=True,
                                          reason="Automatic on emmc join request approval",
                                          createtoken=createaccount_token,
                                          createreturnurl='https://emmc.info/wikix_csa/')
                flash(u'Request approved and MediaWiki user [%s]'
                      u' created successfully.' % jrequest.mw_username,
                      category="success")
            except mwclient.errors.APIError as error:
                if error.code == 'userexists':
                    flash('Account already exists, skipping account creation step.')
                else:
                    #first_response = wiki.api('createaccount', name=jrequest.mw_username, email=jrequest.email,realname=full_name,mailpassword=True,reason="Automatic on emmc join request approval",language='en')
                    #result = wiki.api('query', meta='tokens', type='createaccount')
                    raise
        else:
            flash('Account creation skipped for update request.')
        user_page = wiki.Pages[u'User:{user}'.format(user=jrequest.mw_username)]
        categories = ''
        for working_group in jrequest.working_groups.split(','):
            categories += u'\n [[Category:{wg}_Members]] '.format(wg=working_group.capitalize())
        user_page.save(u"{old_text} \n {body} \n {categories}".format(
            old_text=u"{{warning| This page is auto-generated, do not edit.}}",
            body=make_user_page(jrequest),
            categories=categories))
        if not jrequest.mw_profile:
            jrequest.mw_profile = \
                u'http://{host}{path}/index.php?title=User:{user}'.format(host=app.config['MEDIA_WIKI'],
                                                                    path=app.config['MEDIA_WIKI_PATH'],
                                                                    user=jrequest.mw_username)
        jrequest.approved = True
        jrequest.approve_date = datetime.datetime.now()
        jrequest.approver_user = session['username']
        db.session.commit()
        flash('User profile page generated.', category='success')
        return redirect(url_for('.details', request_id=request_id))
    except Exception as error:
        raise error
        flash(u'Error updating mediawiki %s' % error, category='error')
        return redirect(url_for('.details', request_id=request_id))


@app.route('/join', methods=['GET', 'POST'])
def join():
    form = JoinForm()
    if form.validate_on_submit():
        join_request = make_request(form)
        db.session.add(join_request)
        db.session.flush()
        # Send confirmation email
        send_confirmation_email(join_request)
        db.session.commit()
        send_notification('New Join Request',
                          """Dear EMMC moderator,

There is a new join request waiting for approval. Please use
the following URL to go directly to the request's page:

{url}

Best regards,
EMMC Team
""".format(url=url_for('.details', request_id=join_request.id, _external=True)))

        flash('A confirmation email is sent to your email address, please confirm it to continue.')
        return render_template('join.html', form=JoinForm())
    elif len(form.errors) > 0:
        flash('Fix errors to continue', category="error")
    return render_template('join.html', form=form)


@app.route('/<string:request_id>/update/', methods=['GET', 'POST'])
@login_required
def update(request_id):
    """
    The update method has two functions. Either it changes an existing join request
    or if the request is approved, it will issue a new join request.
    :param request_id:
    :return:
    """
    jrequest = JoinRequest.query.get_or_404(request_id)
    if not _is_approver_user() and jrequest.mw_username != session['username']:
        # No mercy for non-admins except the issuer user
        abort(403)
    #db.session.expunge(jrequest)
    #from sqlalchemy.orm.session import make_transient
    # http://docs.sqlalchemy.org/en/rel_0_8/orm/session.html?highlight=make_transient#sqlalchemy.orm.session.make_transient
    #make_transient(jrequest)
    # Workaround to load the form and put the original attributes back afterwards
    modelling_interests = jrequest.modelling_interests
    email = jrequest.email
    stakeholders = jrequest.stakeholders
    working_groups = jrequest.working_groups
    jrequest.modelling_interests = jrequest.modelling_interests.split(',')
    jrequest.stakeholders = jrequest.stakeholders.split(',')
    jrequest.working_groups = jrequest.working_groups.split(',')
    form = JoinForm(obj=jrequest)
    if form.validate_on_submit():
        jrequest.modelling_interests = modelling_interests
        jrequest.stakeholders = stakeholders
        jrequest.working_groups = working_groups
        updated_request = None
        # If the request is approved we issue a new join request, otherwise we allow the user to change it in place.
        if jrequest.approved:
            updated_request = make_request(form)
            updated_request.mw_username = jrequest.mw_username
            updated_request.mw_profile = jrequest.mw_profile
            updated_request.is_update = True
        else:
            updated_request = make_request(form, jrequest=jrequest)
        db.session.add(updated_request)
        db.session.flush()
        if email != updated_request.email:
            send_confirmation_email(updated_request)
            updated_request.email_confirmed = False
            flash('Please confirm your email address in order to process your updates.')
        else:
            updated_request.email_confirmed = True
        db.session.commit()
        flash('Your request successfully registered. We will contact you soon, thank you for your interest.')
        return render_template('update.html', form=JoinForm(), request=jrequest)
    elif len(form.errors) > 0:
        flash('Fix errors to continue', category="error")
    return render_template('update.html', form=form, request=jrequest)


def get_serializer(secret_key=None):
    if secret_key is None:
        secret_key = app.secret_key
    return URLSafeSerializer(secret_key)


@app.route('/requests/confirm/<string:payload>')
def confirm_email(payload):
    s = get_serializer()
    try:
        request_id = s.loads(payload)
    except BadSignature:
        abort(404)

    request = JoinRequest.query.get_or_404(request_id)
    request.email_confirmed = True
    db.session.commit()
    flash('Your email is now confirmed, we will contact you soon. Thank you for your interest.')
    return redirect(url_for('index'))


def get_activation_link(request):
    s = get_serializer()
    payload = s.dumps(request.id)
    return url_for('confirm_email', payload=payload, _external=True)


def send_confirmation_email(request):
    me = "wikix@emmc.info"
    you = request.email

    # Create message container - the correct MIME type is multipart/alternative.
    msg = MIMEMultipart('alternative')
    msg['Subject'] = "EMMC Confirmation Email"
    msg['From'] = me
    msg['To'] = you

    # Create the body of the message (a plain-text and an HTML version).
    text = """Dear {first_name} {last_name},\n
Thank you for your interest in joining EMMC. We have received your join request. To continue processing
your request we need to confirm your email address. Please click on the link below or copy/paste it into
your browser address bar:\n
{confirmation_link}\n

Upon receiving your confirmation we will reach out to you soon.

Thank you,
EMMC team

    """
    html = """\
    <html>
      <head></head>
      <body>
        <p>Dear {first_name} {last_name},<br><br>
           Thank you for your interest in joining EMMC. We have received your join request. To continue processing
           your request we need to confirm your email address. Please click on the link below or copy/paste it into
           your browser address bar:<br><br>
           <a href="{confirmation_link}">Confirmation link</a><br><br>

           Upon receiving your confirmation we will reach out to you soon.<br><br>

           Thank you,<br>
           EMMC team
        </p>
      </body>
    </html>
    """
    text = text.format(first_name=request.first_name,
                       last_name=request.last_name,
                       confirmation_link=get_activation_link(request))
    html = html.format(first_name=request.first_name,
                       last_name=request.last_name,
                       confirmation_link=get_activation_link(request))
    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(text, 'plain')
    part2 = MIMEText(html, 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)

    # Send the message via SMTP server.
    host = app.config.get('SMTP_HOST', 'localhost')
    port = app.config.get('SMTP_PORT', 25)
    s = smtplib.SMTP(host, port)
    # sendmail function takes 3 arguments: sender's address, recipient's address
    # and message to send - here it is sent as one string.
    s.sendmail(me, you, msg.as_string())
    s.quit()


def send_notification(subject, message):
    sender = app.config.get('REPLYTO_EMAIL', 'wikix@emmc.info')
    receivers = app.config.get('MODERATOR_EMAILS')
    if not receivers or len(receivers) == 0:
        app.logger.error('MODERATOR_EMAILS list is empty or not defined in configs.')
        return
    # Create message container
    msg = MIMEText(message, 'plain')
    msg['Subject'] = subject

    # Send the message via SMTP server.
    host = app.config.get('SMTP_HOST', 'localhost')
    port = app.config.get('SMTP_PORT', 25)
    s = smtplib.SMTP(host, port)
    # Notify each moderator about the message
    # sendmail function takes 3 arguments: sender's address, recipient's address
    # and message to send - here it is sent as one string.
    s.sendmail(sender, receivers, msg.as_string())
    s.quit()
