__author__ = 'sade'

# Recaptcha keys for localhost. Change as appropriate.
RECAPTCHA_PUBLIC_KEY = '6LeYIbsSAAAAACRPIllxA7wvXjIE411PfdB2gt2J'
RECAPTCHA_PRIVATE_KEY = '6LeYIbsSAAAAAJezaIq3Ft_hSTo0YtyeFG-JgRtu'
RECAPTCHA_PARAMETERS = {'hl': 'zh', 'render': 'explicit'}
RECAPTCHA_DATA_ATTRS = {'theme': 'dark'}

import os
_basedir = os.path.abspath(os.path.dirname(__file__))

SECRET_KEY = 'A random string should appear hear.'
SESSION_COOKIE_PATH = "/"
SESSION_COOKIE_NAME = "wikix"

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(_basedir, 'data.db')

# Mediawiki installation endpoint. Do not enter protocol such as http or https.
# Samples: '217.160.180.187' or 'wiki-emmc.org'
MEDIA_WIKI = 'localhost'
MEDIA_WIKI_PATH = '/'

ROWS_PER_PAGE = 20

# Reply to address, will appear as sender in emails
REPLYTO_EMAIL = "wikix@emmc.info"

# List of Emails to be notified of join requests
MODERATOR_EMAILS = []

# Make sure the right domain name appears here, otherwise things will go wrong
SERVER_NAME = "example.com"
DEBUG = True
SQLALCHEMY_TRACK_MODIFICATIONS = False
