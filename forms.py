from urllib.parse import urlparse, urljoin
from flask import request, redirect, url_for, session
from flask_wtf import FlaskForm, RecaptchaField
from wtforms.fields.html5 import URLField
from wtforms.fields import HiddenField
import wtforms as wtf
import constants
import re


class SingleEmailValidator(wtf.validators.Regexp):
    """
    Validates 99% of emails on the internet.
    Regex is originally from http://www.regular-expressions.info/email.html with some modification.

    :param message:
        Error message to raise in case of a validation error.
    """
    def __init__(self, message=None):
        super(SingleEmailValidator, self).__init__(r'^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,63}$', re.IGNORECASE, message)

    def __call__(self, form, field):
        p = re.compile(r'^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,63}$', re.IGNORECASE)
        if not p.match(field.data):
            raise wtf.validators.ValidationError('Invalid email address.')


class JoinForm(FlaskForm):
    """
    Join form.
    """
    first_name = wtf.StringField('First Name', [wtf.validators.DataRequired(),
                                                wtf.validators.Length(min=1, max=50)])
    last_name = wtf.StringField('Last Name', [wtf.validators.DataRequired(),
                                              wtf.validators.Length(min=1, max=100)])
    affiliation = wtf.StringField('Affiliation', [wtf.validators.DataRequired(),
                                                  wtf.validators.Length(min=1, max=100)])
    affiliation_country = wtf.SelectField('Country of your affiliation',
                                          [wtf.validators.DataRequired()],
                                          choices=[(c, c) for c in constants.countries])
    nationality = wtf.SelectField('Nationality',
                                  [wtf.validators.DataRequired()],
                                  choices=[(c, c) for c in constants.countries])
    email = wtf.StringField('Email', [wtf.validators.DataRequired(),
                                      SingleEmailValidator()])
    phone_number = wtf.StringField('Phone number',
                                   [wtf.validators.Optional()])
    website = URLField('Website', validators=[wtf.validators.URL(),
                                              wtf.validators.Optional()])
    # Modeling interests
    modelling_interests = \
        wtf.SelectMultipleField('Modelling interests (multiselect)',
                                [wtf.validators.DataRequired()],
                                choices=[('electronic', 'Electronic'),
                                         ('atomistic', 'Atomistic'),
                                         ('mesoscopic', 'Mesoscopic'),
                                         ('continiuum', 'Continiuum'),
                                         ('process_and_device', 'Process and device'),
                                         ('support', 'Support (measurements, characterisation, '
                                                     'visualisation, HPC etc.)')])
    # Affiliation type
    affiliation_type = wtf.RadioField('Choose whether public research or industry',
                                      [wtf.validators.DataRequired()],
                                      choices=[('academy', 'Academy (universities and other research institutes)'),
                                               ('industry', 'Industry')])
    # Stakeholders
    stakeholders = wtf.SelectMultipleField('Stakeholder type (multiselect)',
                                           [wtf.validators.DataRequired()],
                                           choices=[('MAN', 'Manufacturing industries ("the end-users") '),
                                                    ('SW0', 'Software owners (who sell/provide materials models to'
                                                            'end-users, including commercial and open source) '),
                                                    ('MOD', 'Materials modellers (academic and '
                                                            'industrial model developers) '),
                                                    ('TRA', 'Translators (service providers doing research '
                                                            'based on materials modelling for industry) '),
                                                    ('SUP', 'Supporting desciplines (experimentalists, database '
                                                            'owners, measurement specialists, HPC specialists, '
                                                            'post processing and visualisation etc.) ')])

    working_groups = wtf.SelectMultipleField('Desired working groups (multiselect)',
                                             [wtf.validators.DataRequired()],
                                             choices=[('atomistic', 'Atomistic Models'),
                                                      ('bds', 'Business Decision Support'),
                                                      ('continuum', 'Continuum Models'),
                                                      ('coupling', 'Coupling and Linking'),
                                                      ('electronic', 'Electronic'),
                                                      ('enduser', 'End-User'),
                                                      ('mesoscopic', 'Mesoscopic'),
                                                      ('model_marketplace', 'Model Marketplace'),
                                                      ('interoperability', 'Interoperability'),
                                                      ('osp', 'Open Simulation Platform'),
                                                      ('software_owner', 'Software Owners'),
                                                      ('translators', 'Translators'),
                                                      ('validation', 'Validation')])

    short_bio = wtf.TextAreaField('Short bio (10-15 lines or less than 500 characters)',
                                  [wtf.validators.Optional(),
                                   wtf.validators.Length(max=500)])
    message = wtf.TextAreaField('Message (10-15 lines or less than 500 characters)',
                                [wtf.validators.Optional(),
                                 wtf.validators.Length(max=500)])
    recaptcha = RecaptchaField()
    toc_accepted = wtf.BooleanField("I accept EMMC privacy policy",
                                    validators=[wtf.validators.DataRequired()])
    submit = wtf.SubmitField('Submit')


def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and \
        ref_url.netloc == test_url.netloc


def get_redirect_target():
    for target in request.values.get('next'), session.get('next'), request.referrer:
        if not target:
            continue
        if is_safe_url(target):
            return target


def redirect_back(endpoint, **values):
    target = request.form['next']
    if not target or not is_safe_url(target):
        target = url_for(endpoint, **values)
    return redirect(target)


class RedirectForm(FlaskForm):
    next = HiddenField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not self.next.data:
            self.next.data = get_redirect_target() or ''

    def redirect(self, endpoint='index', **values):
        if is_safe_url(self.next.data):
            return redirect(self.next.data)
        target = get_redirect_target()
        # Clean 'next' flag from session.
        session.pop('next', None)
        return redirect(target or url_for(endpoint, **values))


class LoginForm(RedirectForm):
    username = wtf.StringField('Username', [wtf.validators.DataRequired()])
    password = wtf.StringField('Password', [wtf.validators.DataRequired()])
