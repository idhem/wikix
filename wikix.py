from flask import Flask, url_for, request, render_template
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config.update(
    DEBUG=True,
    SECRET_KEY='Replace this in config file.',
    RECAPTCHA_PUBLIC_KEY = '6LeYIbsSAAAAACRPIllxA7wvXjIE411PfdB2gt2J',
	RECAPTCHA_PRIVATE_KEY = '6LeYIbsSAAAAAJezaIq3Ft_hSTo0YtyeFG-JgRtu',
	MEDIA_WIKI = 'localhost',
	MEDIA_WIKI_PATH = '/',
	REPLYTO_EMAIL = "noreply@emmc.info",
	MODERATOR_EMAILS = []
)
try:
	app.config.from_object('config')
except:
	pass
db = SQLAlchemy(app)


@app.teardown_appcontext
def teardown(exception=None):
    db.session.remove()


def url_for_other_page(page):
    args = request.view_args.copy()
    args['page'] = page
    return url_for(request.endpoint, **args)
app.jinja_env.globals['url_for_other_page'] = url_for_other_page


#@app.errorhandler(404)
def not_found(error):
    return render_template('404.html'), 404


import views
