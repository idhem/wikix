import functools
from flask import session, flash, redirect, request, url_for


def login_required(func):
    @functools.wraps(func)
    def new_func(*args, **kwargs):
        if not session.get('logged_in'):
            flash('You need to login to access this page.', category='warning')
            session['next'] = request.base_url
            return redirect(url_for('login'))
        else:
            return func(*args, **kwargs)
    return new_func
