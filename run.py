from wikix import app as application

if __name__ == '__main__':
    application.Testing = True
    application.run(debug=True)
