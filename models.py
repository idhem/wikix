from wikix import db


class JoinRequest(db.Model):
    __tablename__ = 'requests'

    def __init__(self,
                 submission_date=None,
                 first_name=None,
                 last_name=None,
                 affiliation=None,
                 affiliation_country=None,
                 nationality=None,
                 email=None,
                 phone_number=None,
                 website=None,
                 modelling_interests=None,
                 affiliation_type=None,
                 stakeholders=None,
                 working_groups=None,
                 short_bio=None,
                 message=None,
                 approved=None,
                 approve_date=None,
                 approver_user=None,
                 mw_profile=None,
                 mw_username=None):
        self.submission_date = submission_date
        self.first_name = first_name
        self.last_name = last_name
        self.affiliation = affiliation
        self.affiliation_country = affiliation_country
        self.nationality = nationality
        self.email = email
        self.phone_number = phone_number
        self.website = website
        self.modelling_interests = modelling_interests
        self.affiliation_type = affiliation_type
        self.stakeholders = stakeholders
        self.working_groups = working_groups
        self.short_bio = short_bio
        self.message = message
        self.approved = approved
        self.approve_date = approve_date
        self.approver_user = approver_user
        self.mw_profile = mw_profile
        self.mw_username = mw_username

    id = db.Column(db.Integer, primary_key=True)
    submission_date = db.Column(db.Date)
    first_name = db.Column(db.String)
    last_name = db.Column(db.String)
    affiliation = db.Column(db.String)
    affiliation_country = db.Column(db.String)
    nationality = db.Column(db.String)
    email = db.Column(db.String)
    phone_number = db.Column(db.String)
    website = db.Column(db.String)
    modelling_interests = db.Column(db.String)
    affiliation_type = db.Column(db.String)
    stakeholders = db.Column(db.String)
    working_groups = db.Column(db.String)
    short_bio = db.Column(db.Text)
    message = db.Column(db.Text)
    approved = db.Column(db.Boolean)
    approve_date = db.Column(db.Date)
    approver_user = db.Column(db.String)
    mw_profile = db.Column(db.String)
    mw_username = db.Column(db.String)
    email_confirmed = db.Column(db.Boolean)
    is_update = db.Column(db.Boolean)
